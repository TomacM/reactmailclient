import React from "react";
import "./App.css";
import LandingPage from "./pages/landingPage";
import Contacts from "./pages/contacts";
import ContactShow from "./pages/contactShow";
import Emails from "./pages/emails";
import Folders from "./pages/folders";
import Error from "./pages/errorPage";
import { Route, Switch, withRouter } from "react-router-dom";
import Account from "./pages/accountEdit";
import AccountShow from "./pages/accountShow";
import Email from "./pages/singleEmail";
import Contact from "./pages/singleContact";
import Navbar from "./components/navbar/Navbar";
import Footer from "./components/footer/footer";
import NewEmail from "./pages/newMail";
import newFolder from "./pages/newFolder"

const App = withRouter(({ location }) => {
  return (
    <>
      {location.pathname !== "/" && location.pathname !== "/error" && (
        <Navbar />
      )}
      <Switch>
        <Route exact path="/" component={LandingPage} />
        <Route exact path="/contacts" component={Contacts} />
        <Route exact path="/emails" component={Emails} />
        <Route exact path="/folders" component={Folders} />
        <Route exact path="/account" component={Account} />
        <Route exact path="/account" component={Account} />
        <Route exact path="/email" component={Email} />
        <Route exact path="/contact" component={Contact} />
        <Route exact path="/accountShow" component={AccountShow} />
        <Route exact path="/newEmail" component={NewEmail} />
        <Route exact path="/contactShow" component={ContactShow} />
        <Route exact path="/contactShow" component={ContactShow} />
        <Route exact path="/newFolder" component={newFolder} />
      </Switch>
      {location.pathname !== "/" && <Footer />}
    </>
  );
});

export default App;
