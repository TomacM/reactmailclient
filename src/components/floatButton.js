import React, { Component } from "react";
import { FaPlus } from "react-icons/fa";
import "../App.css";

class FloatBtn extends Component {
  render() {
    return (
      <div className="float">
        <FaPlus className="floatI" />
      </div>
    );
  }
}

export default FloatBtn;
