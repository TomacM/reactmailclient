import React, { Component } from "react";
import { FaPencilAlt } from "react-icons/fa";
import "../App.css";

class FloatEdit extends Component {
  render() {
    return (
      <div className="float">
        <FaPencilAlt className="floatI" />
      </div>
    );
  }
}

export default FloatEdit;
