import React, { Component } from "react";
import { FaSave } from "react-icons/fa";
import "../App.css";

class FloatSave extends Component {
  render() {
    return (
      <div className="float1">
        <FaSave className="floatS" />
      </div>
    );
  }
}

export default FloatSave;
