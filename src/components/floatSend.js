import React, { Component } from "react";
import { FaPaperPlane } from "react-icons/fa";
import "../App.css";

class FloatSend extends Component {
  render() {
    return (
      <div className="float1">
        <FaPaperPlane className="floatS" />
      </div>
    );
  }
}

export default FloatSend;
