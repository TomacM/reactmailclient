import React, { Component } from "react";
import { FaFacebookF } from "react-icons/fa";
import { FaInstagram } from "react-icons/fa";
import { FaLinkedinIn } from "react-icons/fa";
import "../../App.css";
export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer-div">
          <ul className="footer-elements">
            <li>
              <a href="https://www.facebook.com/milos.tomac">
                <FaFacebookF />
              </a>
            </li>
            <li>
              <a href="https://www.instagram.com/tomac_m/">
                <FaInstagram />
              </a>
            </li>
            <li>
              <a href="https://www.linkedin.com/in/milos-tomac-0924091a4/">
                <FaLinkedinIn />
              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
