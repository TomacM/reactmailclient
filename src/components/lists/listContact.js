import React, { Component } from "react";
import { Link } from "react-router-dom";
import kontakt from "../../img/kontakt.png";
import { FaTrashAlt } from "react-icons/fa";
import { GrEdit } from "react-icons/gr";
import { AiFillCaretLeft } from "react-icons/ai";
import "../../App.css";
import axios from "axios";

export default class ListContact extends Component {
  state = {
    contacts: [],
    stanje: false
  };
  delete(contact){

    var r = window.confirm('Are you sure you wish to delete contact: '+contact.display+"?")
    if (r == true) {
      axios
      .delete(
        "http://localhost:8080/contacts/delete/" +
          // "http://localhost:8080/messages/account/" +
          contact.id
      )
      .then((response) => {
        
        this.setState({contacts: this.state.contacts.filter(function(con) { 
          return con !== contact 
      })});
  
      })
      .catch(function (error) {
        alert(error);
      });
    }
  }
  async componentDidMount() {

    axios
    .get(
      "http://localhost:8080/indexs/reindex-contact"
    )
    .then((response) => {
    })
    .catch(function (error) {
      console.log(error);
    });

    if(localStorage.getItem("opcija")=="none"){
      console.log("nista")
    axios
      .get(
        "http://localhost:8080/contacts/account/" +
          localStorage.getItem("username")
      )
      .then((response) => {
        this.setState({ contacts: response.data });
      })
      .catch(function (error) {
        console.log(error);
      });
    }else if((localStorage.getItem("opcija")=="name")){
      var simpleQuery = {
        field: "firstName",
        value: localStorage.getItem("rec")
      };
      if (/\s/.test(localStorage.getItem("rec"))) {
        console.log("pharse");
        axios
        .post(
          "http://localhost:8080/search/contact/phrase",
          simpleQuery
        )
        .then((response) => {
          this.setState({ contacts: response.data });
          this.state.contacts.forEach(element => {
            console.log(element.id)
          });
          
        })
        .catch(function (error) {
          console.log(error);
        });
      }else{
        console.log("term");
      axios
        .post(
          "http://localhost:8080/search/contact/term",
          simpleQuery
        )
        .then((response) => {
          this.setState({ contacts: response.data });
          this.state.contacts.forEach(element => {
            console.log(element.id)
          });
          
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    }
    else if((localStorage.getItem("opcija")=="lastname")){
      var simpleQuery = {
        field: "lastName",
        value: localStorage.getItem("rec")
      };
      if (/\s/.test(localStorage.getItem("rec"))) {
        console.log("phrase");
        axios
        .post(
          "http://localhost:8080/search/contact/phrase",
          simpleQuery
        )
        .then((response) => {
          this.setState({ contacts: response.data });
          this.state.contacts.forEach(element => {
            console.log(element.id)
          });
          
        })
        .catch(function (error) {
          console.log(error);
        });
      }else{
        console.log("term");

      axios
        .post(
          "http://localhost:8080/search/contact/term",
          simpleQuery
        )
        .then((response) => {
          this.setState({ contacts: response.data });
          this.state.contacts.forEach(element => {
            console.log(element.id)
          });
          
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    }else if((localStorage.getItem("opcija")=="note")){

      var simpleQuery = {
        field: "note",
        value: localStorage.getItem("rec")
      };
      if (/\s/.test(localStorage.getItem("rec"))) {
        console.log("pharse");
        axios
        .post(
          "http://localhost:8080/search/contact/phrase",
          simpleQuery
        )
        .then((response) => {
          this.setState({ contacts: response.data });
          this.state.contacts.forEach(element => {
            console.log(element.id)
          });
          
        })
        .catch(function (error) {
          console.log(error);
        });
      }else{
        console.log("term");
      axios
        .post(
          "http://localhost:8080/search/contact/term",
          simpleQuery
        )
        .then((response) => {
          this.setState({ contacts: response.data });
          this.state.contacts.forEach(element => {
            console.log(element.id)
          });
          
        })
        .catch(function (error) {
          console.log(error);
        });
      }
    }
  }
  clickon(contact) {
    localStorage.setItem("contact", contact.id);
    window.location.replace("/contactShow")
  }
  edit(contact){
    localStorage.setItem("contact", contact.id);
    localStorage.setItem("edit", "yes");
    window.location.replace("/contact");
  }


  render() {
    if (this.state.contacts.length === 0) {
      return <div>no contacts</div>;
    }
    return (
      <div>
        {this.state.contacts.map((contact) => (
          <Link
            className="linkLista"
            onDoubleClick={this.clickon.bind(this, contact)}
          >
            <div className="email-lista">
              <img className="kontaktSlika" src={kontakt}></img>
              <span className="naziviContact">
                <b>{contact.firstName+" "+contact.lastName}</b>
     
              </span>
              
            
              <FaTrashAlt className="deleteC"onClick={this.delete.bind(this,contact)}></FaTrashAlt>
              <br/>
               <GrEdit className="editC"onClick={this.edit.bind(this,contact)}></GrEdit>
            </div>

          </Link>
        ))}
      </div>
    );
  }
}
