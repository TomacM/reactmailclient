import React, { Component } from "react";
import { IoIosMailUnread } from "react-icons/io";
import { IoIosMailOpen } from "react-icons/io";
import { FaTrashAlt } from "react-icons/fa";
import { Link } from "react-router-dom";

import axios from "axios";
import { getElementError } from "@testing-library/react";
export default class ListMails extends Component {

  state = {
    messages: [],
    mouse:false
  };
  async componentDidMount() {
    console.log(localStorage.getItem("opcija2")+" "+localStorage.getItem("rec2"))
    axios
    .get(
      "http://localhost:8080/indexs/reindex-messages"
    )
    .then((response) => {
    })
    .catch(function (error) {
      console.log(error);
    });
    if(localStorage.getItem("opcija2")=="none"){
    if(localStorage.getItem("sort")==="up"){
       console.log(localStorage.getItem("sort"))
    axios
      .get(
        "http://localhost:8080/messages/account/sortDes/" +
          // "http://localhost:8080/messages/account/" +
          localStorage.getItem("username")
      )
      .then((response) => {
        this.setState({ messages: response.data });
      })
      .catch(function (error) {
        alert(error);
      });
  }else if(localStorage.getItem("sort")==="down"){
    console.log(localStorage.getItem("sort"))
    axios
    .get(
      "http://localhost:8080/messages/account/sortAsc/" +
        // "http://localhost:8080/messages/account/" +
        localStorage.getItem("username")
    )
    .then((response) => {
      this.setState({ messages: response.data });
    })
    .catch(function (error) {
      alert(error);
    });
  }
}else if((localStorage.getItem("opcija2")=="subject")){
  var simpleQuery = {
    field: "subject",
    value: localStorage.getItem("rec2")
  };
  if (/\s/.test(localStorage.getItem("rec2"))) {
    console.log("pharse");
    axios
    .post(
      "http://localhost:8080/search/email/phrase",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }else{
    console.log("term");
  axios
    .post(
      "http://localhost:8080/search/email/term",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}
else if((localStorage.getItem("opcija2")=="from")){
  var simpleQuery = {
    field: "from",
    value: localStorage.getItem("rec2")
  };
  if (/\s/.test(localStorage.getItem("rec2"))) {
    console.log("pharse");
    axios
    .post(
      "http://localhost:8080/search/email/phrase",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }else{
    console.log("term");
  axios
    .post(
      "http://localhost:8080/search/email/term",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}
else if((localStorage.getItem("opcija2")=="to")){
  var simpleQuery = {
    field: "to",
    value: localStorage.getItem("rec2")
  };
  if (/\s/.test(localStorage.getItem("rec2"))) {
    console.log("pharse");
    axios
    .post(
      "http://localhost:8080/search/email/phrase",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }else{
    console.log("term");
  axios
    .post(
      "http://localhost:8080/search/email/term",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}
else if((localStorage.getItem("opcija2")=="content")){
  var simpleQuery = {
    field: "content",
    value: localStorage.getItem("rec2")
  };
  if (/\s/.test(localStorage.getItem("rec2"))) {
    console.log("pharse");
    axios
    .post(
      "http://localhost:8080/search/email/phrase",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }else{
    console.log("term");
  axios
    .post(
      "http://localhost:8080/search/email/term",
      simpleQuery
    )
    .then((response) => {
      this.setState({ messages: response.data });
      this.state.messages.forEach(element => {
        console.log(element.id)
      });
      
    })
    .catch(function (error) {
      console.log(error);
    });
  }
}
}

  clickon(mail) {
    localStorage.setItem("mail", mail.id);
    window.location.replace("/email")
    axios
    .put(
      "http://localhost:8080/messages/viewed/"+mail.id
    )
    .then((response) => {
      this.setState({ messages: response.data });
    })
    .catch(function (error) {
      alert(error);
    });

  }
  delete(mail){
   
    var r = window.confirm('Are you sure you wish to delete message: '+mail.subject+"?")
    if (r == true) {
          this.setState({messages: this.state.messages.filter(function(message) { 
              return message !== mail 
          })});
    }
  }

  render() {
    console.log(this.state.messages.length);
    if (this.state.messages.length === 0) {
      return <div>loading...</div>;
    } else {
      return (
        <div>
          {this.state.messages.map((mail) => (
            <Link
              className="linkLista"
              onDoubleClick={this.clickon.bind(this, mail)}
              key={mail.id}
            >
              <div className="email-lista">
                <div>
                  {mail.procitano ?    <IoIosMailOpen className="ikonica" />: <IoIosMailUnread className="ikonica" />}
                </div>
                <span className="nazivi">
                  {mail.from.length > 14
                    ? mail.from.toLowerCase().substring(0, 15) + "..."
                    : mail.from}
                </span>
                <span className="subject">
                  <b>
                    {mail.subject.length > 19
                      ? mail.subject.substring(0, 20) + "..."
                      : mail.subject}
                  </b>
                  <span className="razmak"> -</span>
                </span>
                <span className="tekst">
                  {mail.content.length > 34
                    ? mail.content.substring(0, 35) + "..."
                    : mail.content}
                </span>
          
                <span className="datum">
                  {mail.dateTime.substring(0, 10) + " "}
                  {mail.dateTime.substring(11, 16)}
                  {"          "} 
                 <FaTrashAlt className="deleteE" onClickCapture={this.delete.bind(this,mail)}></FaTrashAlt>
                </span>
              </div>
            
            </Link>
          ))}
        </div>
      );
    }
  }
}
