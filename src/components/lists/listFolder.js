import React, { Component } from "react";
import { FaFolder } from "react-icons/fa";
import { Link } from "react-router-dom";
import { FaTrashAlt } from "react-icons/fa";

import axios from "axios";
export default class ListFolders extends Component {
  state = {
    folders: [],
  };
  delete(folder){
    
       
    var r = window.confirm('Are you sure you wish to delete contact: '+folder.name+"?")
    if (r == true) {
      axios
      .delete(
        "http://localhost:8080/folders/" +
          // "http://localhost:8080/messages/account/" +
          folder.id
      )
      .then((response) => {
        this.setState({folders: this.state.folders.filter(function(fold) { 
          return fold !== folder 
        })});
      })
      .catch(function (error) {
        alert(error);
      });
    }
  }
  async componentDidMount() {
    axios
      .get(
        "http://localhost:8080/folders/account/" +
          localStorage.getItem("username")
      )
      .then((response) => {
        this.setState({ folders: response.data });
      })
      .catch(function (error) {
        alert(error);
      });
  }
  render() {
    if (this.state.folders.length === 0) {
      return <div>no folders</div>;
    }
    return (
      <div>
        {this.state.folders.map((folder) => (
      <Link className="linkLista" to="/folders">
        <div className="email-lista">
          <FaFolder className="ikonica1" />
          <span className="naziviFoldera">
        <b>{folder.name}</b>
          </span>
          <span className="foldersNumberOf">{folder.messages.length}<FaTrashAlt className="deleteF" onClick={this.delete.bind(this,folder)}></FaTrashAlt></span> 
        </div>
      </Link>
             ))}
      </div>
    );
  }
}
