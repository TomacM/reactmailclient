import React from "react";
import loginImg from "../../img/login.svg";
import axios from "axios";

export class Login extends React.Component {
  constructor(props) {
    super(props);
    localStorage.setItem("token", "");
    this.state = {
      username: "",
      password: "",
    };
  }

  handleChangeUsername = (event) => {
    this.setState({
      username: event.target.value,
    });
  };

  handleChangePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };

  handleEnterKeyPress = (event) => {
    if (event.key === "Enter") {
      this.handleSubmit(event);
    }
  };
  handleSubmit = (event) => {
    if (this.state.username === "") {
      alert("Username field is empty");
    } else if (this.state.password === "") {
      alert("Passowrd field is empty");
    } else {
      axios
        .get(
          "http://localhost:8080/accounts/login/" +
            this.state.username +
            "/" +
            this.state.password,

          {
            username: this.state.username,
            password: this.state.password,
          }
        )
        .then((response) => {
          localStorage.setItem("token", "mrkela");
          localStorage.setItem("sort","up")
          localStorage.setItem("user_id", response.data.id);
          localStorage.setItem("username", response.data.username);

          try {
            window.location.replace("/emails");
          } catch (e) {
            alert(e);
          }
        })
        .catch(function (error) {
          alert("Check your username or password!");
        });
    }

    event.preventDefault();
  };
  redirect = () => {
    this.props.history.push("/registration");
  };
  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="content">
          <h1>Tim 15</h1>
          <div className="image">
            <img src={loginImg} alt="login" />
          </div>
          <div className="form">
            <div className="header">Login</div>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                id="username"
                value={this.state.username}
                type="text"
                onKeyPress={this.handleEnterKeyPress}
                onChange={this.handleChangeUsername}
                name="username"
                autoComplete="off"
                placeholder="username"
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                id="password"
                type="password"
                name="password"
                placeholder="password"
                value={this.state.password}
                autoComplete="off"
                onKeyPress={this.handleEnterKeyPress}
                onChange={this.handleChangePassword}
              />
              <button onClick={this.handleSubmit} type="submit" className="btn">
                Login
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
