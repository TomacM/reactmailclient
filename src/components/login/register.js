import React from "react";
import loginImg from "../../img/login.svg";
import axios from "axios";

export class Register extends React.Component {
  
  handleChangeUsername = (event) => {
    console.log(event.target.value)
    localStorage.setItem("un",event.target.value)

  };
  
  handleChangePassword = (event) => {
    console.log(event.target.value)
    localStorage.setItem("p",event.target.value)
  };
  
  handleChangeRepassword = (event) => {
    localStorage.setItem("rp",event.target.value)
  };
  reg(){
    axios
    .get(
      "http://localhost:8080/accounts/getacc/" +
        localStorage.getItem("un")
    )
    .then((response) => {
      localStorage.setItem("bol",response.data)
    })
    .catch(function (error) {
      alert(error);
    });
    console.log(localStorage.getItem("bol"))

     if (localStorage.getItem("un") == "") {
      alert("Username field is empty");
    } else if (localStorage.getItem("p")== "") {
      alert("Passowrd field is empty");
    }else if (localStorage.getItem("rp") !== localStorage.getItem("p")) {
      alert("Repassword must equal password");}   
    else {

      axios
        .post(
          "http://localhost:8080/accounts/registrationUser/" +
          localStorage.getItem("un") +
            "/" +
            localStorage.getItem("p")+"/ime"+"/prezime",
        )
        .then((response) => {
          window.location.replace("/")
          alert("welcome");
          try {
          } catch (e) {
            alert(e+"1");
          }
        })
        .catch(function (error) {
          alert(error+"2");
        });
    }}
  


  

  render() {
    return (
      <div className="base-container" ref={this.props.containerRef}>
        <div className="content">
          <h1>Tim 15</h1>
          <div className="image">
            <img src={loginImg} alt="Icon" />
          </div>
          <div className="form">
            <div className="header">Register</div>
            <div className="form-group">
              <label htmlFor="username">Username</label>
              <input
                type="text"
                name="username"
                placeholder="username"
                onChange={this.handleChangeUsername}
                autoComplete="off"
              />
            </div>
            <div className="form-group">
              <label htmlFor="password">Password</label>
              <input
                type="password"
                name="password"
                onChange={this.handleChangePassword}
                placeholder="password"
                minLength="8"
              />
            </div>
            <div className="form-group">
              <label htmlFor="re-password">re-password</label>
              <input
                type="password"
                name="re-password"
                onChange={this.handleChangeRepassword}
                placeholder="re-password"
                minLength="8"
              />
              <button type="button" onClick={this.reg}className="btn">
                Register
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
