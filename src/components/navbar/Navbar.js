import React, { Component } from "react";
import { Link } from "react-router-dom";
import { FaAlignLeft } from "react-icons/fa";
import loginImg from "../../img/login.svg";
import { FaPowerOff } from "react-icons/fa";
import { FaFolder } from "react-icons/fa";
import { FaPhoneAlt } from "react-icons/fa";
import { FaUserAlt } from "react-icons/fa";

export default class Navbar extends Component {
  state = {
    isOpen: false,
  };

  handleToggle = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };
  render() {
    if (localStorage.getItem("token") !== "") {
      return (
        <nav className="navbar">
          <div className="nav-center">
            <div className="nav-header">
              <Link to="/emails" className="emailLink">
                <img className="imgStyle" src={loginImg} alt="Home" />
              </Link>
            </div>
            <Link>
              <button
                type="button"
                className="nav-btn"
                onClick={this.handleToggle}
              >
                <FaAlignLeft className="nav-icon" />
              </button>
            </Link>
            <ul
              className={this.state.isOpen ? "nav-links show-nav" : "nav-links"}
            >
              <li>
                <Link to="/folders" className='navFolders'>
                  <FaFolder />
                  <span className='navFolders'>
                  Folders</span>
                </Link>
              </li>
              <li>
                <Link to="/contacts">
                  <FaPhoneAlt />
                  <span className='navContacts'>
                  Contacts</span>
                </Link>
              </li>
              <li>
                <Link to="/accountShow">
                  <FaUserAlt />
                  <span className='navAcc'>
                  Account</span>
                </Link>
              </li>
              <li className={this.state.isOpen ? "" : "logout-link"}>
                <Link to="/">
                    <FaPowerOff></FaPowerOff>
                    <span className='navLog'>
                  Logout</span>
                </Link>
              </li>
            </ul>
          </div>
        </nav>
      );
    } else {
      return null;
    }
  }
}
