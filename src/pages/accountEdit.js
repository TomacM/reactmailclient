import React from "react";
import "../App.css";
import FloatSave from "../components/floatButtonSave";
import image from "../img/userslika.png";
import history from "./../history";
import ErrorPage from "./errorPage";
import axios from "axios";

export default class Account extends React.Component {
  state = {
    username: "",
    id: "",
    smtpAddress: "",
    password: "",
    repassword: "",
    messages: [],
  };

  async componentDidMount() {
    console.log(localStorage.getItem("user_id"));
    axios
      .get(
        "http://localhost:8080/accounts/username/" +
          localStorage.getItem("username")
      )
      .then((response) => {
        this.setState({
          username: response.data.username,
          password: response.data.password,
          id: response.data.id,
          smtpAddress: response.data.smtpAddress,
          messages: response.data.messages,
        });
      })
      .catch(function (error) {
        alert(error);
      });
  }
  changePassword = (event) => {
    this.setState({
      password: event.target.value,
    });
  };
  changerePassword = (event) => {
    this.setState({
      repassword: event.target.value,
    });
  };
  handleSubmit = () => {
    if (this.state.password.trim() !== this.state.repassword.trim()) {
      alert("re-password must equals password");
    } else if (this.state.password.trim() === "") {
      alert("Password field is empty");
    } else {
      axios({
        method: "put",
        url:
          "http://localhost:8080/accounts/" + localStorage.getItem("user_id"),
        data: {
          password: this.state.password,
          id: this.state.id,
          smtpAddress: this.state.smtpAddress,
          username: this.state.username,
          messages: this.state.messages,
        },
      })
        .then((response) => {
          alert("Update done");
          window.location.replace("/emails");
        })
        .catch(function (error) {
          console.log(error);
        });
    }
  };

  render() {
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
              <button
                className="dugmeSave"
                type="button"
                onClick={this.handleSubmit}
              >
                <FloatSave />
              </button>
              <ul className="accountForm">
                <li>
                  <label className="labeli ">Username</label>
                </li>
                <li>
                  <input
                    className="input1"
                    type="text"
                    value={this.state.username}
                    disabled
                  />
                </li>
                <li>
                  <label className="labeli ">Password</label>
                </li>
                <li>
                  <input
                    className="input1"
                    type="password"
                    value={this.state.password}
                    onChange={this.changePassword}
                  />
                </li>
                <li>
                  <label className="labeli ">Re-password</label>
                </li>
                <li>
                  <input
                    className="repasswordInput input1"
                    type="password"
                    onChange={this.changerePassword}
                  />
                </li>
              </ul>
              <img className="userslika" src={image}></img>
              <input type="file"></input>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");
      return <ErrorPage />;
    }
  }
}
