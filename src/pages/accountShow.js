import React from "react";
import "../App.css";
import FloatSave from "../components/floatButtonEdit";
import image from "../img/userslika.png";
import { Link } from "react-router-dom";
import history from "./../history";
import ErrorPage from "./errorPage";
import axios from "axios";

export default class AccountShow extends React.Component {
  state = {
    username: "",
    password: "",
    email: "",
  };
  async componentDidMount() {
    axios
      .get(
        "http://localhost:8080/accounts/username/" +
          localStorage.getItem("username")
      )
      .then((response) => {
        this.setState({
          username: response.data.username,
          password: response.data.password,
          email: response.data.smtpAddress + "@gmail.com",
        });
      })
      .catch(function (error) {
        alert(error);
      });
  }
  render() {
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
              <Link to="/account">
                <button className="dugmeSave" type="button">
                  <FloatSave />
                </button>
              </Link>
              <img className="userslikaa" src={image}></img>
              <ul className="accountForm">
                <li>
                  <label className="labeli ">Username</label>
                </li>
                <li>
                  <input
                    className="firstNameInput input1"
                    type="text"
                    disabled
                    value={this.state.username}
                  />
                </li>
                <li>
                  <label className="labeli ">Password</label>
                </li>
                <li>
                  <input
                    className="lastNameInput input1"
                    type="password"
                    disabled
                    value={this.state.password}
                  />
                </li>
                <li>
                  <label className="labeli ">Email </label>
                </li>
                <li>
                  <input
                    className="emailInput input1"
                    type="text"
                    disabled
                    value={this.state.email}
                  />
                </li>
              </ul>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");
      return <ErrorPage />;
    }
  }
}
