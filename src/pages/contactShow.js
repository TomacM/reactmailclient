import React from "react";
import "../App.css";
import image from "../img/userslika.png";
import history from "./../history";
import ErrorPage from "./errorPage";
import axios from "axios";

class ContactShow extends React.Component {
  state = {
    firstname: "",
    lastname: "",
    email: "",
    note: "",
    displayname: "",
    id: "",
  };

  componentDidMount() {
    console.log(localStorage.getItem("contact"));

    axios
      .get("http://localhost:8080/contacts/" + localStorage.getItem("contact"))
      .then((response) => {
        this.setState({
          id: response.data.id,
          firstname: response.data.firstName,
          lastname: response.data.lastName,
          email: response.data.email,
          note: response.data.note,
          displayname: response.data.display,
        });
      })
      .catch(function (error) {
        alert(error);
      });
  }


  render() {
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
              <img className="userslika" src={image}></img>
              <ul className="emailForm">
                <li>
                  <label className="labeli lab">
                    <span className="content-name">First name</span>
                  </label>
                </li>
                <li>
                  <input
                    className="firstNameInput input1"
                    type="text"
                    disabled
                    value={this.state.firstname}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="lastname" className="content-name">
                      Last name
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="lastNameInput input1"
                    type="text"
                    disabled
                    value={this.state.lastname}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="name" className="content-name">
                      Mail
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="emailInput input1"
                    type="text"
                    disabled
                    value={this.state.email}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="name" className="content-name">
                      Display name
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="emailInput input1"
                    type="text"
                    disabled
                    value={this.state.displayname}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="note" className="content-name">
                      Note
                    </span>
                  </label>
                </li>
                <li>
                  <textarea
                    className="noteInput input1"
                    rows="5"
                    cols="30"
                    disabled
                    value={this.state.note}
                  ></textarea>
                </li>
              </ul>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");
      return <ErrorPage />;
    }
  }
}
export default ContactShow;
