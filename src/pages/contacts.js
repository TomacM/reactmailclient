import React from "react";
import "../App.css";
import ListMails from "../components/lists/listContact";
import FloatBtn from "../components/floatButton";
import { Link } from "react-router-dom";
import history from "./../history";
import ErrorPage from "./errorPage";
import { GrAddCircle} from "react-icons/gr";
import { FcSearch} from "react-icons/fc";
import { RiRestartLine} from "react-icons/ri";



export class Contacts extends React.Component {

    state = {
      username: "",
      preg:"none",
    };  
    componentDidMount(){
      localStorage.setItem("opcija",'none');
      localStorage.setItem("restart",false)
    }
    handleChange(event) {
      let value = event.target.value;
      this.setState({
        preg:value,
      });
      localStorage.setItem("pretraga",value)
      
  }
  changeSearch = (event) => {
    localStorage.setItem("word",event.target.value);
    console.log(localStorage.getItem("word"))
  };
    selectprovera(){
      if(localStorage.getItem("pretraga")=="none"){
        alert("Chooise some search option")
      }
      if(localStorage.getItem("pretraga")=="name"){
        localStorage.setItem("opcija","name");
        localStorage.setItem("rec",localStorage.getItem("word"));
        window.location.replace("/contacts")
      }
      if(localStorage.getItem("pretraga")=="lastname"){
        localStorage.setItem("opcija","lastname");
        localStorage.setItem("rec",localStorage.getItem("word"));
        window.location.replace("/contacts")
      }  if(localStorage.getItem("pretraga")=="note"){
        localStorage.setItem("rec",localStorage.getItem("word"));
        localStorage.setItem("opcija","note");
        window.location.replace("/contacts")

      }
    }
    klik(){
      localStorage.setItem("pretraga","none");
      window.location.replace("/contacts")
    }


  

  render() {
  
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
           
              <select className="pretraga"  value={localStorage.getItem("pretraga")}
        onChange={(e) => this.handleChange(e)} >
                <option value="none">Select search option</option>
                <option value="name">Firstname</option>
                <option value="lastname">Lastname</option>
                <option value="note">Note</option>
              </select>   
            <input type="text" className="pretragaInput" placeholder="Search" onChange={this.changeSearch }/>
            <a className="search-btn" onClick={this.selectprovera}><FcSearch/></a>
            <button className="restart" onClick={this.klik}><RiRestartLine></RiRestartLine></button>
              <div className="lista2">
              <Link to="contact" className="addContact"><GrAddCircle onClick={ localStorage.setItem("edit", "no")}/></Link>
              <span className="displayContacts">Display</span>
                <ListMails />
              </div>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");

      return <ErrorPage />;
    }
  }
}
export default Contacts;
