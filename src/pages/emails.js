import React from "react";
import "../App.css";
import { Link } from "react-router-dom";
import ListMails from "../components/lists/listEmail";
import LandingPage from "../pages/landingPage";
import history from "./../history";
import ErrorPage from "./errorPage";
import { GrAddCircle} from "react-icons/gr";
import { AiFillCaretUp
} from "react-icons/ai";
import { AiFillCaretDown
} from "react-icons/ai";
import { FcSearch} from "react-icons/fc";
import { RiRestartLine} from "react-icons/ri";
import { FaSortUp } from "react-icons/fa";

export default class Emails extends React.Component {
  state = {
    username: "",
    preg:"none",
  };  
  sortup(){
    localStorage.setItem("sort","up")
    window.location.replace("/emails")

  }
  componentDidMount(){
    localStorage.setItem("opcija2",'none');
    localStorage.setItem("restart",false)
  }
  handleChange(event) {
    let value = event.target.value;
    this.setState({
      preg:value,
    });
    localStorage.setItem("pretraga",value)
    
}
changeSearch = (event) => {
  localStorage.setItem("word2",event.target.value);
  console.log(localStorage.getItem("word2"))
};
selectprovera(){
  if(localStorage.getItem("pretraga")=="none"){
    alert("Chooise some search option")
  }
  if(localStorage.getItem("pretraga")=="subject"){
    localStorage.setItem("opcija2","subject");
    localStorage.setItem("rec2",localStorage.getItem("word2"));
    window.location.replace("/emails")
  }
  if(localStorage.getItem("pretraga")=="from"){
    localStorage.setItem("opcija2","from");
    localStorage.setItem("rec2",localStorage.getItem("word2"));
    window.location.replace("/emails")
  }  if(localStorage.getItem("pretraga")=="to"){
    localStorage.setItem("rec2",localStorage.getItem("word2"));
    localStorage.setItem("opcija2","to");
    window.location.replace("/emails")

  }
  if(localStorage.getItem("pretraga")=="content"){
    localStorage.setItem("rec2",localStorage.getItem("word2"));
    localStorage.setItem("opcija2","content");
    window.location.replace("/emails")

  }
}
klik(){
  localStorage.setItem("pretraga","none");
  window.location.replace("/emails")
}
  sortdown(){
    localStorage.setItem("sort","down")
    window.location.replace("/emails")
  }
  render() {
  if (localStorage.getItem("token") !== "") {
    return (
      <div className="App">
        <div className="div1">
          <div className="body-div">
          <select className="pretraga"  value={localStorage.getItem("pretraga")}
        onChange={(e) => this.handleChange(e)} >
                <option value="none">Select search option</option>
                <option value="subject">Subject</option>
                <option value="from">From</option>
                <option value="to">To</option>
                <option value="content">Email content</option>
              </select>   
              <input type="text" className="pretragaInput" placeholder="Search" onChange={this.changeSearch }/>
            <a className="search-btn" onClick={this.selectprovera}><FcSearch/></a>
            <button className="restart" onClick={this.klik}><RiRestartLine></RiRestartLine></button>
            {/* <Link to="/newEmail">
              <FloatBtn className="dugmeAdd" />
            </Link> */}
            <div className="lista2">
            <Link to="newEmail" className="addEmail"><GrAddCircle /></Link>
              <span className="from">From</span><span className="naslovi">Subject</span><span className="msg">Message</span><span className="datetime">Date time
             </span> <button className="sortup" onClick={this.sortup}><AiFillCaretUp/></button><button onClick={this.sortdown} className="sortdown"><AiFillCaretDown/></button>
              <ListMails />

            </div>
          </div>
        </div>
      </div>
    );
  } else if (localStorage.getItem("token") === "") {
    history.push("/error");
    return <ErrorPage />;
  }
}
};
