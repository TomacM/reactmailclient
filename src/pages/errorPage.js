import React from "react";
import "../App.css";
import { Link } from "react-router-dom";

const ErrorPage = () => {
  return (
    <div className="App">
      hello from error page
      <Link to="/">Login</Link>
    </div>
  );
};
export default ErrorPage;
