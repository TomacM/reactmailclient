import React from "react";
import "../App.css";
import ListMails from "../components/lists/listFolder";
import { Link } from "react-router-dom";
import history from "./../history";
import ErrorPage from "./errorPage";
import { GrAddCircle} from "react-icons/gr";

const Folders = () => {
  if (localStorage.getItem("token") !== "") {
    return (
      <div className="App">
        <div className="div1">
          <div className="body-div">
            <div className="lista">
            <Link to="newFolder" className="addEmail"><GrAddCircle /></Link><span className="folderName">Folder name</span><span className="folderContent">Folder content</span>
              <ListMails />
            </div>
          </div>
        </div>
      </div>
    );
  } else if (localStorage.getItem("token") === "") {
    history.push("/error");
    return <ErrorPage />;
  }
};
export default Folders;
