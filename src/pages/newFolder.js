import React from "react";
import "../App.css";
import FloatSave from "../components/floatButtonSave";
import image from "../img/userslika.png";
import history from "./../history";
import ErrorPage from "./errorPage";
import axios from "axios";

class newFolder extends React.Component {
  state = {
      name:"",
  };

  changeName = (event) => {
    this.setState({
      name: event.target.value,
    });
  };
  handleSubmit = () => {
      
      if (this.state.name === "") {
        alert("Name field is empty");
     
      } else {
        var folder = {
          name: this.state.name
        };
        axios
          .post(
            "http://localhost:8080/folders/" +
              localStorage.getItem("username"),
            folder
          )
          .then((response) => {
            alert("Done");
            window.location.replace("/folders");
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    
  };

  render() {
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
              <button
                className="dugmeSave"
                type="button"
                onClick={this.handleSubmit}
              >
                <FloatSave />
              </button>
              <ul className="emailForm">
                <li>
                  <label className="labeli lab">
                    <span for="name" className="content-name">
                      Folder name
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="firstNameInput input1"
                    type="text"
                    onChange={this.changeName}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="lastname" className="content-name">
                        Condition
                    </span>
                  </label>
                  <select className="cbCon">
                        <option value="FROM">FROM</option>
                        <option value="TO">TO</option>
                        <option value="CC">CC</option>
                        <option value="BCC">BCC</option>
                        </select>
                </li>

                <li>
                  <label className="labeli">
                    <span for="lastname" className="content-name">
                      Operation    
                    </span>
                  
                  </label>
                  <select className="cbOp">
                        <option value="Move">Move</option>
                        <option value="Copy">Copy</option>
                        <option value="Delete">Delete</option>
                        </select>
                </li>
              </ul>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");
      return <ErrorPage />;
    }
  }
}
export default newFolder;
