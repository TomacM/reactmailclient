import React from "react";
import "../App.css";
import { Link } from "react-router-dom";
import FloatSend from "../components/floatSend";
import history from "./../history";
import ErrorPage from "./errorPage";

const NewEmail = () => {
  if (localStorage.getItem("token") !== "") {
    return (
      <div className="App">
        <div className="div1">
          <div className="body-div">
            <Link to="/emails">
              <FloatSend className="dugmeAdd" />
            </Link>
            <ul className="emailForm">
              <li>
                <label className="labeli">To</label>
              </li>
              <li>
                <input className="toInput input1" type="email" />
              </li>
              <li>
                <label className="labeli">Subject</label>
              </li>
              <li>
                <input className="subjectInput input1" type="text" />
              </li>
              <li>
                <label className="labeli">Message </label>
              </li>
              <li>
                <textarea
                  className="messageInput input1"
                  rows="10"
                  cols="30"
                ></textarea>
              </li>
              <li>
                <label className="labeli">Attachment</label>
              </li>
              <li>
                <input className="input1" type="file"></input>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  } else if (localStorage.getItem("token") === "") {
    history.push("/error");
    return <ErrorPage />;
  }
};
export default NewEmail;
