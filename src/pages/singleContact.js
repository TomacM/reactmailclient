import React from "react";
import "../App.css";
import FloatSave from "../components/floatButtonSave";
import image from "../img/userslika.png";
import history from "./../history";
import ErrorPage from "./errorPage";
import axios from "axios";

class Contact extends React.Component {
  state = {
    firstname: "",
    lastname: "",
    email: "",
    note: "",
    displayname: "",
    id: "",
  };

  componentDidMount() {
    console.log(localStorage.getItem("edit"));

    axios
      .get("http://localhost:8080/contacts/" + localStorage.getItem("contact"))
      .then((response) => {
        if (localStorage.getItem("edit") === "yes") {
          this.setState({
            id: response.data.id,
            firstname: response.data.firstName,
            lastname: response.data.lastName,
            email: response.data.email,
            note: response.data.note,
            displayname: response.data.display,
          });
        }
      })
      .catch(function (error) {
        alert(error);
      });
  }
  changeFN = (event) => {
    this.setState({
      firstname: event.target.value,
    });
  };
  changeDN = (event) => {
    this.setState({
      displayname: event.target.value,
    });
  };
  changeLN = (event) => {
    this.setState({
      lastname: event.target.value,
    });
  };
  changeMail = (event) => {
    this.setState({
      email: event.target.value,
    });
  };
  changeNote = (event) => {
    this.setState({
      note: event.target.value,
    });
  };

  handleSubmit = () => {
    if (localStorage.getItem("edit") === "yes") {
      if (this.state.firstname === "") {
        alert("firstname field is empty");
      } else if (this.state.lastname === "") {
        alert("Lastname field is empty");
      } else if (this.state.email === "") {
        alert("email field is empty");
      } else if (this.state.displayname === "") {
        alert("displayname field is empty");
      } else {
        axios({
          method: "put",
          url:
            "http://localhost:8080/contacts/" +
            this.state.id +
            "/" +
            localStorage.getItem("username"),
          data: {
            id: this.state.id,
            firstName: this.state.firstname,
            lastName: this.state.lastname,
            display: this.state.displayname,
            email: this.state.email,
            note: this.state.note,
          },
        })
          .then((response) => {
            alert("Update done");
            window.location.replace("/contacts");
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    } else if (localStorage.getItem("edit") === "no") {
      if (this.state.firstname === "") {
        alert("firstname field is empty");
      } else if (this.state.lastname === "") {
        alert("Lastname field is empty");
      } else if (this.state.email === "") {
        alert("email field is empty");
      } else if (this.state.displayname === "") {
        alert("displayname field is empty");
      } else {
        var contact = {
          id: this.state.id,
          firstName: this.state.firstname,
          lastName: this.state.lastname,
          display: this.state.displayname,
          email: this.state.email,
          note: this.state.note,
        };
        axios
          .post(
            "http://localhost:8080/contacts/" +
              localStorage.getItem("username"),
            contact
          )
          .then((response) => {
            alert("Add new contact done");
            window.location.replace("/contacts");
          })
          .catch(function (error) {
            console.log(error);
          });
      }
    }
  };

  render() {
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
              <button
                className="dugmeSave"
                type="button"
                onClick={this.handleSubmit}
              >
                <FloatSave />
              </button>
              <ul className="emailForm">
                <li>
                  <label className="labeli lab">
                    <span for="name" className="content-name">
                      First name
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="firstNameInput input1"
                    type="text"
                    value={this.state.firstname}
                    onChange={this.changeFN}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="lastname" className="content-name">
                      Last name
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="lastNameInput input1"
                    type="text"
                    value={this.state.lastname}
                    onChange={this.changeLN}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="lastname" className="content-name">
                      Displayname
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="input1"
                    type="text"
                    value={this.state.displayname}
                    onChange={this.changeDN}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="name" className="content-name">
                      Mail
                    </span>
                  </label>
                </li>
                <li>
                  <input
                    className="emailInput input1"
                    type="email"
                    value={this.state.email}
                    onChange={this.changeMail}
                  />
                </li>
                <li>
                  <label className="labeli">
                    <span for="note" className="content-name">
                      Note
                    </span>
                  </label>
                </li>
                <li>
                  <textarea
                    className="noteInput input1"
                    rows="5"
                    value={this.state.note}
                    onChange={this.changeNote}
                    cols="30"
                  ></textarea>
                </li>
              </ul>
              <img className="userslika" src={image}></img>
              <input type="file"></input>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");
      return <ErrorPage />;
    }
  }
}
export default Contact;
