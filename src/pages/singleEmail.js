import React from "react";
import "../App.css";
import history from "./../history";
import ErrorPage from "./errorPage";
import axios from "axios";

class Email extends React.Component {
  state = {
    onClickEdit: false,
    from: "",
    subject: "",
    message: "",
    attachment: "",
  };
  componentDidMount() {
    console.log(localStorage.getItem("mail"));
    axios
      .get("http://localhost:8080/messages/" + localStorage.getItem("mail"))
      .then((response) => {
        this.setState({
          from: response.data.from,
          subject: response.data.subject,
          message: response.data.content,
          attachment: response.data.attachment,
          procitano: response.data.procitano
        });
      })
      .catch(function (error) {
        alert(error);
      });
  }
  render() {
    console.log(this.state.procitano)
    if (localStorage.getItem("token") !== "") {
      return (
        <div className="App">
          <div className="div1">
            <div className="body-div">
              <ul className="emailForm">
                <li>
                  <label className="labeli">From</label>
                </li>
                <li>
                  <input
                    className="fromInput input1"
                    type="email"
                    disabled
                    value={this.state.from}
                  />
                </li>
                <li>
                  <label className="labeli">Subject</label>
                </li>
                <li>
                  <input
                    className="subjectInput input1"
                    type="text"
                    disabled
                    value={this.state.subject}
                  />
                </li>
                <li>
                  <label className="labeli">Message </label>
                </li>
                <li>
                  <textarea
                    className="messageInput input1"
                    rows="10"
                    cols="30"
                    disabled
                    value={this.state.message}
                  ></textarea>
                </li>
                <li>
                  <label className="labeli">Attachment</label>
                </li>
                <li>
                  <textarea
                    className="attachmentInput input1"
                    rows="5"
                    cols="30"
                    disabled
                    value={this.state.attachment}
                  ></textarea>
                </li>
              </ul>
            </div>
          </div>
        </div>
      );
    } else if (localStorage.getItem("token") === "") {
      history.push("/error");
      return <ErrorPage />;
    }
  }
}
export default Email;
